//
//  LoginController
//  ParseChat
//
//  Created by Paulo Cesar on 21/10/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class LoginController: UITableViewController {
    
    @IBOutlet weak var done: UIBarButtonItem!
    
    @IBOutlet weak var t_username: UITextField!
    @IBOutlet weak var t_password: UITextField!
    
    @IBOutlet weak var message_cell: UITableViewCell!
    @IBOutlet weak var message_label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        //already logged in
        if PFUser.currentUser() != nil {
            self.performSegueWithIdentifier("next", sender: self)
        }
        else {
            t_username.becomeFirstResponder()
        }
    }
    
    @IBAction func nextFromUsername(sender: AnyObject) {
        t_password.becomeFirstResponder()
    }
    
    @IBAction func done(sender: AnyObject) {
        if valid() {
            logIn(self)
        }
    }
    
    @IBAction func changed(sender: AnyObject) {
        update_done()
    }
    
    func update_done() {
        done.enabled = valid()
    }
    
    func valid() -> Bool {
        return (t_username.text?.characters.count >= 1 &&
                t_password.text?.characters.count >= 1)
    }
    
    
    @IBAction func logIn(sender: AnyObject) {
        if valid() {
            message_cell.hidden = false
            message_label.text = "logging in"
            
            PFUser.logInWithUsernameInBackground(t_username.text!, password:t_password.text!) {
                (user: PFUser?, error: NSError?) -> Void in
                if user != nil {
                    self.message_cell.hidden = true
                    self.performSegueWithIdentifier("next", sender: self)
                }
                else {
                    self.message_label.text = error?.localizedDescription
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        self.performSegueWithIdentifier("next", sender: self)
    }

}
