//
//  ChatController.swift
//  ParseChat
//
//  Created by Paulo Cesar on 22/10/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class ChatController: UIViewController, UIWebViewDelegate, UIAlertViewDelegate {

    var contact : PFUser?
    
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var text_field: UITextField!
    @IBOutlet weak var send_button: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var exclude_messages : [String] = []
    
    var html = "<style>p { margin: 10px };</style>"
    
    var timer : NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let username = contact?.username {
            navigationItem.title = "Chat with "+username
        }
        
        webview.delegate = self
        updateMessages()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: self.view.window)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "finish", name: block_notification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedNotification:", name: message_notification, object: nil)
    }
    
    func receivedNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if userInfo["type"] as? String == type_message {
                let contactId = userInfo["userId"] as! String
                if contactId == contact!.objectId {
                    updateMessages()
                }
            }
        }
    }
    
    @IBAction func sendMessage(sender: AnyObject) {
        
        text_field.enabled = false
        send_button.enabled = false
        
        let message = PFObject(className: "Message")
        message["from"] = PFUser.currentUser()!
        message["to"] = contact!
        message["content"] = text_field.text
        
        message.saveInBackgroundWithBlock { success, error in
            if success {
                self.appendMessage(PFUser.currentUser()!, message: self.text_field.text!)
                self.notify(self.text_field.text!)
                self.text_field.text = ""
                
                self.exclude_messages.append(message.objectId!)
            }
            else {
                print(error?.localizedDescription)
            }
            
            self.text_field.enabled = true
            self.send_button.enabled = true
        }
    }
    
    func notify(message: String) {
        let pushQuery = PFInstallation.query()
        pushQuery?.whereKey("user", equalTo: contact!)
        
        let data = [
            "alert": PFUser.currentUser()!.username!+": "+message,
            "type": type_message,
            "userId": PFUser.currentUser()!.objectId!
        ]
        
        let push = PFPush()
        push.setQuery(pushQuery!)
        push.setData(data)
        push.sendPushInBackground()
    }
    
    func appendMessage(user: PFUser, message: String) {
        let style = PFUser.currentUser()! == user ? "" : "style='text-align: right'"
        html += "<p "+style+">"+user.username!+": "+message+"</p>"
        webview.loadHTMLString(html, baseURL: nil)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        webview.scrollView.scrollRectToVisible(CGRectMake(0, webview.scrollView.contentSize.height-1, 1, 1), animated: false)
    }
    
    func updateMessages() {
        print("update")
        let query = PFQuery(className: "Message")
        
        query.whereKey("objectId", notContainedIn: exclude_messages)
        query.whereKey("to", containedIn: [PFUser.currentUser()!, contact!])
        query.whereKey("from", containedIn: [PFUser.currentUser()!, contact!])
        
        let yesterday = NSDate(timeIntervalSinceNow: -60*60*24)
        query.whereKey("createdAt", greaterThan: yesterday)
        
        query.orderByAscending("createdAt")
        
        query.findObjectsInBackgroundWithBlock({ objects, error in
            if objects != nil {
                for message in objects! {
                    if let from = message["from"] as? PFUser, content = message["content"] as? String {
                        self.appendMessage(from, message: content)
                        self.exclude_messages.append(message.objectId!)
                    }
                }
            }
            else {
                print(error?.localizedDescription)
            }
        })
    }

    @IBAction func reload(sender: AnyObject) {
        updateMessages()
    }
    
    @IBAction func blockUser(sender: AnyObject) {
        blockWarning()
    }
    
    func blockWarning() {
        UIAlertView(title: "Block user",
            message: "Are you sure you want to block "+contact!.username!+"?",
            delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes").show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            //block user
            let currentUser = PFUser.currentUser()
            let relation = currentUser?.relationForKey("blocked_contacts")
            relation?.addObject(contact!)
            currentUser?.saveInBackgroundWithBlock({ (success, error) -> Void in
                if success {
                    self.notifyBlock()
                    dispatch_async(dispatch_get_main_queue(), {
                        self.finish()
                    })
                }
                else {
                    print(error)
                }
            })
        }
    }
    
    func finish() {
        self.performSegueWithIdentifier("finish", sender: self)
    }
    
    func notifyBlock() {
        let pushQuery = PFInstallation.query()
        pushQuery?.whereKey("user", equalTo: contact!)
        
        let data = [
            "alert": "You were blocked by "+PFUser.currentUser()!.username!,
            "type": type_block,
            "userId": PFUser.currentUser()!.objectId!
        ]
        
        let push = PFPush()
        push.setQuery(pushQuery!)
        push.setData(data)
        push.sendPushInBackground()
    }
    
    func keyboardWillShow(sender: NSNotification) {
        bottomConstraint.constant = 260
        view.setNeedsUpdateConstraints()
    }
    
    func keyboardWillHide(sender: NSNotification) {
        bottomConstraint.constant = 0
        view.setNeedsUpdateConstraints()
    }
    

}
