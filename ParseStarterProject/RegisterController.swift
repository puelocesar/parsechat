//
//  RegisterController.swift
//  ParseChat
//
//  Created by Paulo Cesar on 21/10/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class RegisterController: UITableViewController {

    @IBOutlet weak var done: UIBarButtonItem!
    
    @IBOutlet weak var t_username: UITextField!
    @IBOutlet weak var t_password: UITextField!
    @IBOutlet weak var t_confirm: UITextField!
    
    @IBOutlet weak var message_cell: UITableViewCell!
    @IBOutlet weak var message_label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PFUser.logOut()
    }
    
    override func viewDidAppear(animated: Bool) {
        t_username.becomeFirstResponder()
    }

    @IBAction func nextFromUsername(sender: AnyObject) {
        t_password.becomeFirstResponder()
    }
    
    @IBAction func nextFromPassword(sender: AnyObject) {
        t_confirm.becomeFirstResponder()
    }
    
    @IBAction func done(sender: AnyObject) {
        if valid() {
            register(self)
        }
    }
    
    @IBAction func changed(sender: AnyObject) {
        update_done()
    }
    
    func update_done() {
        done.enabled = valid()
    }
    
    func valid() -> Bool {
        return (t_username.text?.characters.count >= 3 &&
           t_password.text?.characters.count >= 1 &&
           t_confirm.text?.characters.count >= 1 &&
           t_password.text == t_confirm.text)
    }
    
    @IBAction func register(sender: AnyObject) {
        if valid() {
            
            message_cell.hidden = false
            message_label.text = "signing up"
            
            let user = PFUser()
            user.username = t_username.text
            user.password = t_confirm.text
            user["registered"] = true
            
            user.signUpInBackgroundWithBlock({ success, error in
                
                if let error = error {
                    let errorString = error.userInfo["error"] as? String
                    self.message_label.text = errorString
                }
                else {
                    self.message_cell.hidden = true
                    self.performSegueWithIdentifier("next", sender: self)
                }
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
