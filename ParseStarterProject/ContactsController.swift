//
//  ContactsController.swift
//  ParseChat
//
//  Created by Paulo Cesar on 21/10/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class ContactsController: UITableViewController, UISearchBarDelegate {

    var query_result : [PFObject]?
    var search_result : [PFObject] = []
    
    var selected_contact : PFUser?
    
    @IBOutlet weak var search_bar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateRegistrationStatus()
        
        search_bar.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateContacts", name: block_notification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateContacts", name: contact_notification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedMessage:", name: message_notification, object: nil)
        
        //configure installation for push
        let installation = PFInstallation.currentInstallation()
        installation["user"] = PFUser.currentUser()!
        installation.saveInBackground()
    }
    
    func receivedMessage(notification: NSNotification) {

        if UIApplication.sharedApplication().applicationState == UIApplicationState.Inactive {
            if let userInfo = notification.userInfo {
                let contactId = userInfo["userId"] as! String
                let contact_query = PFUser.query()!
                
                contact_query.getObjectInBackgroundWithId(contactId, block: { (contact, error) -> Void in
                    if contact != nil {
                        self.selected_contact = contact as? PFUser
                        self.performSegueWithIdentifier("chat", sender: self)
                    }
                    else {
                        print(error?.localizedDescription)
                    }
                })
            }
        }
        else {
            //show message?
        }
    }
    
    func updateRegistrationStatus() {
        let user = PFUser.currentUser()
        if let registered = user?["registered"] as? Bool {
            if registered {
                updateContacts()
            }
            else {
                goToRegistration()
            }
        }
        else {
            goToRegistration()
        }
    }
    
    func updateContacts() {
        
        let block_relation = PFUser.currentUser()?.relationForKey("blocked_contacts")
        block_relation?.query()?.findObjectsInBackgroundWithBlock({ (blocked_users, error) -> Void in
            if error == nil {
                self.getContacts(blocked_users as! [PFUser])
            }
            else {
                self.getContacts([])
                print(error?.localizedDescription)
            }
        })
    }
    
    func getContacts(blocked_contacts: [PFUser]) {
        let contact_relation = PFUser.currentUser()?.relationForKey("contacts")
        let query = contact_relation?.query()
        
        //current user was blocked by other user
        let inner_query = PFUser.query()
        inner_query?.whereKey("objectId", equalTo: PFUser.currentUser()!.objectId!)
        query?.whereKey("blocked_contacts", doesNotMatchQuery: inner_query!)
        
        //current user blocked
        query?.whereKey("objectId", notContainedIn: blocked_contacts.map({ c in c.objectId! }))
        
        query?.findObjectsInBackgroundWithBlock({ objects, error in
            if error == nil && objects != nil {
                
                self.query_result = objects!
                self.search_result = self.query_result!
                self.tableView.reloadData()
            }
            else {
                self.search_result = []
                print(error?.localizedDescription)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Search
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            search_result = (query_result != nil) ? query_result! : []
        }
        else if query_result != nil {
            search_result = query_result!.filter({ c in
                (c["username"] as! String).lowercaseString.containsString(searchText.lowercaseString)
            })
        }
        
        tableView.reloadData()
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return search_result.count
    }

    func getUserForIndexPath(indexPath: NSIndexPath) -> PFUser? {
        return search_result[indexPath.row] as? PFUser
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("standard", forIndexPath: indexPath)

        // Configure the cell...
        if let user = getUserForIndexPath(indexPath) {
            cell.textLabel?.text = user.username
        }

        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let user = getUserForIndexPath(indexPath) {
            selected_contact = user
            performSegueWithIdentifier("chat", sender: self)
        }
    }

    // MARK: - Navigation

    @IBAction func logout(sender: AnyObject) {
        PFUser.logOutInBackground()
        performSegueWithIdentifier("logout", sender: self)
    }
    
    func goToRegistration() {
        performSegueWithIdentifier("register", sender: self)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let chat_controller = segue.destinationViewController as? ChatController {
            chat_controller.contact = selected_contact
        }
    }
    
    @IBAction func unwindToContacts(segue: UIStoryboardSegue) {
        updateContacts()
    }

}
