//
//  AddContactController.swift
//  ParseChat
//
//  Created by Paulo Cesar on 21/10/15.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class AddContactController: UITableViewController {

    private var searchField : UITextField?
    var result_count = 0
    var query_result : [PFObject]?
    var currentContacts : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateCurrentContactIds()
    }
    
    func updateCurrentContactIds() {
        let currentUser = PFUser.currentUser()
        let relation = currentUser?.relationForKey("contacts")
        
        relation?.query()?.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
            self.currentContacts = objects?.map({ u in u.objectId! })
        })
    }
    
    func search() {
        if searchField?.text?.characters.count > 0 {
            let query = PFUser.query()
            
            query?.whereKey("objectId", notEqualTo: PFUser.currentUser()!.objectId!)
            query?.whereKey("username", containsString: searchField?.text)
            
            if currentContacts != nil {
                query?.whereKey("objectId", notContainedIn: currentContacts!)
            }
            
            query?.findObjectsInBackgroundWithBlock({ objects, error in
                if objects != nil {
                    self.result_count = objects!.count
                    self.query_result = objects
                    
                    self.tableView.reloadData()
                }
            })
        }
        else {
            result_count = 0
            tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
            return 1
        }
        else {
            return result_count
        }
    }
    
    func getUserForIndexPath(indexPath: NSIndexPath) -> PFUser? {
        return query_result?[indexPath.row] as? PFUser
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell

        // Configure the cell...
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("search", forIndexPath: indexPath)
            
            for view in cell.subviews[0].subviews {
                if let text = view as? UITextField {
                    searchField = text
                    text.addTarget(self, action: "search", forControlEvents: .PrimaryActionTriggered)
                }
            }
        }
        else {
            cell = tableView.dequeueReusableCellWithIdentifier("result", forIndexPath: indexPath)
            if let user = getUserForIndexPath(indexPath) {
                cell.textLabel?.text = user.username
            }
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Search contact"
        }
        else {
            return "Results"
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let user = getUserForIndexPath(indexPath) {
            
            let currentUser = PFUser.currentUser()
            let relation = currentUser?.relationForKey("contacts")
            relation?.addObject(user)
            currentUser?.saveInBackgroundWithBlock({ (success, error) -> Void in
                if success {
                    self.notify(user)
                    dispatch_async(dispatch_get_main_queue(), {
                        self.performSegueWithIdentifier("finish", sender: self)
                    })
                }
                else {
                    print(error)
                }
            })
        }
    }
    
    func notify(user: PFUser) {
        let pushQuery = PFInstallation.query()
        pushQuery?.whereKey("user", equalTo: user)
        
        let data = [
            "alert": PFUser.currentUser()!.username!+" just added you as a contact",
            "type": type_contact,
            "userId": PFUser.currentUser()!.objectId!
        ]
        
        let push = PFPush()
        push.setQuery(pushQuery!)
        push.setData(data)
        push.sendPushInBackground()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
